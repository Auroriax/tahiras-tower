# Tahira's Tower by @Auroriax

![tahira-isles.gif](https://bitbucket.org/repo/R6pX9A/images/2128518959-tahira-isles.gif)

## Play at:
[GameJolt](http://gamejolt.com/games/tahira-s-tower/151274), [itch.io](https://amazingcookie.itch.io/tahiras-tower)

## About
Tahira's Tower is a love letter to the beautiful puzzle simplicity of Sokoban. Tahira's Tower adds the concept of 3D and gravity to the formula of the box pushing puzzle game, which allows for more than a dozen of quality brain benders.

The framework for this game is inspired by Stephen Lavelle's [Puzzlescript](https://www.puzzlescript.net/). It internally keeps track of the current level state and supports undo, multiple camera angles, a free camera, and a state manager that progresses and validates all player moves.

## Open source
You could use this to:

* Find out how Babylon.js works and what you can do with it.
* Make new puzzles! Let me know it if you make a cool one! In-game you can import and export puzzles as a string with [F8].
* I can imagine you making a new 3D puzzle game out of this! Change some rules, I'd love to see what you could come up with!

## Contact
[Twitter](https://twitter.com/Auroriax), [Website](http://auroriax.com/)